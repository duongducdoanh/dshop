<?php
/**
 * UserFixture
 *
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'mail_address' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 255, 'unsigned' => false),
		'phone_number' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 13, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'sex' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 2, 'unsigned' => false),
		'birthday' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'province_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'district_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'commune_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'house_number' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'street' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'disable_flag' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'username' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'mail_address' => 1,
			'phone_number' => 'Lorem ipsum',
			'sex' => 1,
			'birthday' => '2015-05-20 04:01:07',
			'province_id' => 1,
			'district_id' => 1,
			'commune_id' => 1,
			'house_number' => 'Lorem ip',
			'street' => 'Lorem ipsum dolor sit amet',
			'disable_flag' => 1,
			'created' => '2015-05-20 04:01:07',
			'modified' => '2015-05-20 04:01:07'
		),
	);

}
