<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css(array('style'));
        echo $this->Html->script(array('common'));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <div id="container">
        <div id="header">
            <div id="banner">
                <?php echo $this->element('banner');?>
            </div>
            <div id="menu">
                <?php echo $this->element('menu');?>
            </div>
            <div id="show-text">
                
            </div>
        </div>
        <div id="content">

            <?php echo $this->Session->flash(); ?>

            <?php echo $this->fetch('content'); ?>
            <div id='sidebar'>
            </div>
            <?php echo $this->element('sidebar');?>
            <div id='slider'>
            </div>
            <?php echo $this->element('slider');?>
            <div id="wrap-content">
            </div>
            <?php echo $this->element('wrap-content');?>
        </div>
        <div id="footer">
            <?php echo $this->element('footer');?>
        </div>
    </div>
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
