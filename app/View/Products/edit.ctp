<div class="products form">
<?php echo $this->Form->create('Product'); ?>
	<fieldset>
		<legend><?php echo __('Edit Product'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('category_id');
		echo $this->Form->input('name');
		echo $this->Form->input('url_image');
		echo $this->Form->input('url_image_2');
		echo $this->Form->input('url_image_3');
		echo $this->Form->input('url_image_4');
		echo $this->Form->input('url_image_5');
		echo $this->Form->input('price');
		echo $this->Form->input('color');
		echo $this->Form->input('material');
		echo $this->Form->input('style');
		echo $this->Form->input('weight');
		echo $this->Form->input('size');
		echo $this->Form->input('quantity');
		echo $this->Form->input('disable_flag');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Products'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
